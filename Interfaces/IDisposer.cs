﻿namespace CrazyRam.Core.Initialization
{
    public interface IDisposer
    {
        void ReleaseResources();
    }
}

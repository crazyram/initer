﻿namespace CrazyRam.Core.Initialization
{
    public interface IInitializer
    {
        void Init();
    }
}

﻿using CrazyRam.Core.Initialization;
using UnityEngine;

namespace CrazyRam.Core
{
    public abstract class SingletonIniter<T> : MonoBehaviour, IInitializer where T : Component
    {
        public static T Instance { get; private set; }

        public virtual void Init()
        {
            Instance = this as T;
        }
    }
}

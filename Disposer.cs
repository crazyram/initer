﻿using System;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Serialization;
using UnityEngine;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Initialization
{
    public class Disposer : MonoBehaviour, IDisposer
    {
        [Serializable]
        private class SerializedDisposser : SerializedComponent<IDisposer>
        {
            [SerializeField, SerializedComponent(typeof(IDisposer))]
            private Component _component;

            protected override Component Component
            {
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                get { return _component; }
            }
        }

        [SerializeField]
        private bool _autoDispose;

        [SerializeField]
        private SerializedDisposser[] _dispossers;

        private void OnDestroy()
        {
            if (_autoDispose)
                ReleaseResources();
        }

        public void ReleaseResources()
        {
            _dispossers.Map(disposser => disposser.GetItem()?.ReleaseResources());
        }
    }
}

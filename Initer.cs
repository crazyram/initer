﻿using System;
using CrazyRam.Core.Helpers;
using CrazyRam.Core.Serialization;
using UnityEngine;
using System.Runtime.CompilerServices;

namespace CrazyRam.Core.Initialization
{
    public class Initer : MonoBehaviour, IInitializer
    {
        [Serializable]
        private class SerializedInterface : SerializedComponent<IInitializer>
        {
            [SerializeField, SerializedComponent(typeof(IInitializer))]
            private Component _component;

            protected override Component Component
            {
                [MethodImpl(MethodImplOptions.AggressiveInlining)]
                get => _component;
            }
        }

        [SerializeField]
        private bool _initOnStart;

        [SerializeField]
        private SerializedInterface[] _initers;

        protected void Awake()
        {
            if (_initOnStart)
                Init();
        }

        public void Init()
        {
            _initers.Map(initializer => initializer.GetItem()?.Init());
        }
    }
}
